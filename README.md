﻿### 采用[JFinal](http://www.jfinal.com/), [Beetl](http://ibeetl.com)/[Rythm](http://rythmengine.org)开发的一个简单的BBS

希望它作为一个JFinal, Beetl和Rythm入门的DEMO，可以帮到新人。（tips：关于Rythm的代码，目前只存在于git历史记录里）

最后，向开源前辈们致敬。

特别鸣谢：
dreamlu：http://www.dreamlu.net

### 部署完毕的效果地址是：http://bbs.dreamlu.net

### 运行方法：导入sql，编译整个项目，配置相应的参数，运行Config.java里的main方法即可；

#### 现在JFinal-BBS还有很多地方有待完善。

### 2.0
转换成maven项目，升级到JFinal2.0 + beetl2.2.3更改为全cookie登陆认证！

### 1.2
修复了小bug；加入了jsoup以抵御xss攻击；加入了自定义路径的支持；

### 1.1
修复了几处bug；增加了外键和删除级联；重构了部分model代码，配合ehcache更省代码，更加方便读取数据；取消了游客的留言权限；增加了后台管理的操作；

### 关于jfinal

* 项目站点：http://www.jfinal.com/

### 关于Beetl

* 项目站点：http://ibeetl.com/
* 交互式体验：http://www.ibeetl.com/beetlonline
* 文档：http://ibeetl.com/guide/

### 关于Rythm

* 项目站点：http://rythmengine.org
* 交互式体验：http://fiddle.rythmengine.org
* 源代码：https://github.com/greenlaw110/rythm

常见问题：
http://bbs.dreamlu.net/topic/49