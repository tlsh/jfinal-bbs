package cn.iver.interceptor;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.core.Controller;

import cn.iver.common.Const;
import cn.iver.common.WebUtils;
import cn.iver.model.User;

/**
 * Created with IntelliJ IDEA.
 * Author: StevenChow
 * Date: 13-4-4
 */
public class AdminInterceptor implements Interceptor {
    @Override
    public void intercept(Invocation inv) {
        Controller controller = inv.getController();
        User user = WebUtils.currentUser(controller);

        if (user != null && Const.ADMIN_EMAIL.equals(user.getStr("email"))){
            inv.invoke();
        }else{
            controller.setAttr("msg", "需要管理员权限");
            controller.renderError(500);
        }
    }

}
